<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <title>Youngkyoon Jang: Research</title>

    <!--
	<meta http-equiv="refresh" content="1; URL=https://sites.google.com/site/youngkyoonjang/home/">
    <meta name="keywords" content="automatic redirection">
	-->

    <link rel="stylesheet" href="stylesheets/styles.css">
    <link rel="stylesheet" href="stylesheets/github-dark.css">
    <script src="javascripts/scale.fix.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="wrapper">
      <header>
        <h1>Youngkyoon Jang, Ph.D.  <a href="http://www.qmul.ac.uk" target="_blank"><img src="images/qmul_logo.jpg" alt="" width="100" height="29" title="QMUL"/></a>
                                <!-- -->
				    <a href="http://www.cam.ac.uk/" target="_blank"><img src="images/cambridge_logo.jpg" alt="" width="110.5" height="29" title="Cambridge"/></a></h1>
				<!-- -->          
        <br>| &nbsp &nbsp<a href="index.html"><strong><span style="color: #069">Home</span></strong>&nbsp &nbsp </a>
        | &nbsp &nbsp <a href="curriculum_vitae.html"><strong><span style="color: #069">C.V.</span></strong>&nbsp &nbsp </a>
        | &nbsp &nbsp <a href="research.html"><strong><span style="color: #069">Research</span> </strong>&nbsp &nbsp </a>
        | &nbsp &nbsp <a href="publication.html"><strong><span style="color: #069">Publication</span> </strong>&nbsp &nbsp </a>
		| &nbsp &nbsp <a href="links.html"><strong><span style="color: #069">Links</span></strong>&nbsp &nbsp |</a>
      </header>
      
<section>
<DIV><span style="font-size: 14pt"><strong>Human Behaviour/Affect Understanding</strong></span></DIV>
<!--
  <div>
    <iframe title="YouTube video player" type="text/html" width="480" height="270" src="https://www.youtube.com/embed/H04gX_xzOgA?ecver=1" frameborder="0" allowfullscreen="true"></iframe>
    <br> 
  </div>
  <div>
    <iframe title="YouTube video player" type="text/html" width="480" height="270" src="https://www.youtube.com/embed/eF6tvzoUwas?ecver=1" frameborder="0" allowfullscreen="true"></iframe>
    <br> 
  </div>
-->
  <div>
    <iframe title="YouTube video player" type="text/html" width="480" height="270" src="https://www.youtube.com/embed/wRhviLO7nug?ecver=1" frameborder="0" allowfullscreen="true"></iframe>
    <br> 
  </div>
  <div>I am currently involved in SensingFeeling project supported by <a href="https://www.gov.uk/government/organisations/innovate-uk" target="_blank" rel="nofollow">Innovative UK</a>. I proposed SmileNet, which is the first smiling face detection network that does not require pre-processing such as face detection and registration in advance to generate a normalised input image.</div>
  <div><br></div>
  <div>Research interests: Deep learning, affective computing, behaviour understanding</div>
  <div><br></div>
  <div>Papers:</div>
  <div>
    <div> - IEEE ICCV Workshop on AMFG, Venice, Italy, Oct. 28, 2017.</div>
  <div> - Download: [pdf] [<a href="https://youtu.be/H04gX_xzOgA" target="_blank" rel="nofollow">demo1</a> / <a href="https://youtu.be/eF6tvzoUwas" target="_blank" rel="nofollow">demo2</a> / <a href="https://youtu.be/wRhviLO7nug" target="_blank" rel="nofollow">demo3</a> on Youtube video] [<a href="https://sites.google.com/view/sensingfeeling/" target="_blank" rel="nofollow">Project page</a>] [<a href="http://sensingfeeling.com/" target="_blank" rel="nofollow">Publicising page</a>]
  </div>
</section>

<section>
<DIV><span style="font-size: 14pt"><strong>Face Landmark Detection/Tracking</strong></span></DIV>
  <div><img src="images/research/SmartMirror.png" alt="" title="SmartMirror"/></div>
  <div> 
    <iframe title="YouTube video player" type="text/html" src="https://www.youtube.com/embed/_49D94KxCNk" frameborder="0" allowfullscreen="true" width="144" height="81"></iframe> , 
    <iframe title="YouTube video player" type="text/html" src="https://www.youtube.com/embed/bCmpwTY1-v4" frameborder="0" allowfullscreen="true" width="144" height="81"></iframe> ,  
    <iframe title="YouTube video player" type="text/html" src="https://www.youtube.com/embed/4REj0yHp8WI" frameborder="0" allowfullscreen="true" width="144" height="81"></iframe>
    <br> 
  </div>
  <div>"Highly Realistic and Human-centric VR Technology Development" project.</div>
  <div><br>
    </div>
</section>

<section>
<DIV><span style="font-size: 14pt"><strong><u><a href="https://sites.google.com/site/fingergesture/fui" target="_blank" rel="nofollow">Metaphoric Hand Gestures in VR</a></u></strong></span></DIV>
  <div>
    <iframe title="YouTube video player" type="text/html" src="https://www.youtube.com/embed/AvsWxkU8q3E" frameborder="0" allowfullscreen="true" width="480" height="270"></iframe>
    <br> 
  </div>
  <div>"Metaphoric Hand Gestures for Orientation-aware VR Object Manipulation with an Egocentric Viewpoint" in collaboration with <a href="http://www.iis.ee.ic.ac.uk/~tkkim/index.html" target="_blank" rel="nofollow">ICVL Lab.</a> (supervised by <a href="http://www.iis.ee.ic.ac.uk/~tkkim/cv.htm" target="_blank" rel="nofollow">Dr.T-K. Kim</a>) of <a href="http://www3.imperial.ac.uk/" target="_blank" rel="nofollow">Imperial College London</a>, UK.)</div>
  <div><br>
    </div>
  <div>Papers:</div>
  <div>
    <div> - IEEE Trans. on Human-Machine Systems (THMS)), vol. 47, no. 1, pp.113-127, Feb. 2017.</div>
    <div> - [<a href="files/my_papers/cvprw16_poster.pdf" target="_blank">Poster</a>] (also presented in IEEE CVPR 2016 Workshop on HANDS, Las Vegas, NV, USA. Jul. 01, 2016 as a poster, and won the best poster award, sponsored by Facebook/Oculus and Purdue University)</div>
    <div> - (also presented in Asia-Pacific Workshop on Mixed Reality (APMR), Andong, Korea, Apr 2016, and won the best presentation award)</div>
    <div> - Download: [<a href="http://ieeexplore.ieee.org/document/7588127/" target="_blank" rel="nofollow">pdf</a>] [<a href="https://youtu.be/AvsWxkU8q3E" target="_blank" rel="nofollow">demo</a>] [<a href="https://sites.google.com/site/fingergesture/fui/" rel="nofollow">Project page</a>]</div>
  </div>
</section>

<section>
<DIV><span style="font-size: 14pt"><strong><u><a href="https://sites.google.com/site/fingergesture/" target="_blank" rel="nofollow">3D Finger Gesture Recognition</a></u></strong></span></DIV>
  <div> 
    <iframe title="YouTube video player" type="text/html" src="https://www.youtube.com/embed/ioesG0K3Pes?rel=0&wmode=opaque" frameborder="0" allowfullscreen="true" width="480" height="270"></iframe>
    <br> 
  </div>
  <div>"3D Finger CAPE: Clicking Action and Position Estimation under Self-Occlusions in Egocentric Viewpoint" - With a collaborator <a href="https://hyungjinchang.wordpress.com/" target="_blank" rel="nofollow">Hyung Jin Chang</a> who was a member of <a href="http://www.iis.ee.ic.ac.uk/~tkkim/index.html" target="_blank" rel="nofollow">ICVL Lab.</a> (supervised by <a href="http://www.iis.ee.ic.ac.uk/~tkkim/cv.htm" target="_blank" rel="nofollow">Dr.T-K. Kim</a>) of <a href="http://www3.imperial.ac.uk/" target="_blank" rel="nofollow">Imperial College London</a>, UK.)</div>
  <div><br>
    </div>
  <div>Papers:</div>
  <div>
    <div> - IEEE Trans. on Vis. and Computer Graphics (TVCG)), vol. 21, no. 4, pp. 501-510, April 2015.</div>
    <div> - (also presented in IEEE VR 2015, Mar. 23-27, 2015 as a long paper, accept rate: 13.8% (13/94))</div>
    <div> - Download: [<a href="http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7014300" target="_blank" rel="nofollow">PDF</a>] [<a href="https://www.youtube.com/watch?v=ioesG0K3Pes" target="_blank">Demo on Youtube video</a>] [<a href="https://sites.google.com/site/fingergesture/" target="_blank" rel="nofollow">Project page</a>]</div>
  </div>
</section>

<section>
  <DIV><span style="font-size: 14pt"><strong>Unified Visual Perception Model for Context-aware Augmented Reality (Multiple Object)</strong></span></DIV>
    <div> 
      <iframe title="YouTube video player" type="text/html" src="https://www.youtube.com/embed/XJSNa_IAjng?rel=0&wmode=opaque" frameborder="0" allowfullscreen="true" width="320" height="240"></iframe>
      </div>
    <div>
      <div>We propose unified visual perception model, which imitates the human visual perception process, for the stable object recognition necessarily required for augmented reality (AR) in the field. The proposed model is designed based on the theoretical bases in the field of cognitive informatics, brain research and psychological science.</div>
      <div> </div>
      <div>Papers:</div>
      <div> - ISMAR 2013: DC program (officially, poster), Adelaide, S.A, Australia, Oct. 1-4, 2013.</div>
      <div> - Download: [<a href="http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=6671818" rel="nofollow" target="_blank">PDF</a>] [<a href="http://www.youtube.com/watch?feature=player_detailpage&v=XJSNa_IAjng" target="_blank">Demo on Youtube video</a>]</div>
      <div> </div>
      </div>
</section>

<section>
<DIV><span style="font-size: 14pt"><strong>Video-based Object Recognition</strong></span></DIV>
  <div>
    <iframe title="YouTube video player" type="text/html" src="https://www.youtube.com/embed/ybADitKTq9A?rel=0&wmode=opaque" frameborder="0" allowfullscreen="true" width="320" height="240"></iframe>
</div>
  <div>"Video-based Object Recognition using Novel Set-of-Sets Representation" - With a collaborator <a href="http://www.iis.ee.ic.ac.uk/~yliu/" target="_blank" rel="nofollow">Yang Liu</a> who is a member of <a href="http://www.iis.ee.ic.ac.uk/~tkkim/index.html" target="_blank" rel="nofollow">ICVL Lab.</a> (supervised by <a href="http://www.iis.ee.ic.ac.uk/~tkkim/cv.htm" target="_blank" rel="nofollow">Dr.T-K. Kim</a>) of <a href="http://www3.imperial.ac.uk/" target="_blank" rel="nofollow">Imperial College London</a>, UK.)</div>
  <div> </div>
  <div>Papers:</div>
  <div> - 3rd Workshop on Egocentric (First-person) Vision (In conjunction with CVPR 2014)</div>
  <div> - Download: [<a href="http://www.cv-foundation.org//openaccess/content_cvpr_workshops_2014/W16/papers/Liu_Video-based_Object_Recognition_2014_CVPR_paper.pdf" target="_blank" rel="nofollow">PDF</a>] [<a href="https://www.youtube.com/watch?v=j_UcFou3hnY" target="_blank">Demo1</a>, <a href="https://www.youtube.com/watch?v=ybADitKTq9A" target="_blank">Demo2</a> on Youtube video]</div>
</section>

<section>
<DIV><span style="font-size: 14pt"><strong>Local Feature Descriptors for 3D Object Recognition</strong></span></DIV>
  <div><img border="0" height="116" src="https://sites.google.com/site/youngkyoonjang/_/rsrc/1353393304441/research/local_angle_pattern.bmp?height=240&width=320" width="155"></div>
  <div>This paper represents 3D object recognition, which is an extension of the common feature point-based object recognition, based on novel descriptors utilizing local angles (for shape), gradient orientations (for texture of corners), and color information. </div>
  <div> </div>
  <div>Papers:</div>
  <div> - ISUVR 2012: <a href="http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6296806" target="_blank" rel="nofollow">PDF</a></div>
</section>

<section>
<DIV><span style="font-size: 14pt"><strong>Semi-automatic ROI Detection for In-Situ Painting Recognition</strong></span></DIV>
  <div><img border="0" height="120" src="https://sites.google.com/site/youngkyoonjang/_/rsrc/1353392569011/research/painting_recognition.png?height=212&width=320" width="182"></div>
  <div>In the case of illumination and view direction changes, the ability to accurately detect the Regions of Interest (ROI) is important for robust recognition. In this paper, we propose a stroke-based semi-automatic ROI detection algorithm using adaptive thresholding and a Hough-transform method for in-situ painting recognition.</div>
  <div> </div>
  <div>Papers:</div>
  <div> - HCI International 2011: <a href="https://docs.google.com/viewer?a=v&pid=sites&srcid=ZGVmYXVsdGRvbWFpbnx5b3VuZ2t5b29uamFuZ3xneDo0NWNlOTI5NGU4NmQ4MTFm" target="_blank">PDF</a>, <a href="http://www.youtube.com/watch?feature=player_detailpage&v=pGp-L2dbcYU" target="_blank">VIDEO</a></div>
</section>

<section>
<DIV><span style="font-size: 14pt"><strong>Computer Vision-based Smile Training System</strong></span></DIV>
  <div><img border="0" height="133" src="https://sites.google.com/site/youngkyoonjang/_/rsrc/1353392008250/research/smile_training.bmp?height=240&width=320" width="178"></div>
  <div>This paper presents an adaptive lip feature point detection algorithm for the proposed real-time smile training system using visual instructions. The proposed algorithm can detect a lip feature point irrespective of lip color with minimal user participation, such as drawing a line on a lip on the screen. Therefore, the proposed algorithm supports adaptive feature detection by real-time analysis for a color histogram. Moreover, we develop a supportive guide model as visual instructions for the target expression. By using the guide model, users can train their smile expression intuitively because they can easily identify the differences between their smile and target expression.</div>
  <div> </div>
  <div>Papers:</div>
  <div> - The 4th International Conference on E-Learning and Games (Edutainment 2009): <a href="http://link.springer.com/content/pdf/10.1007%2F978-3-642-03364-3_46" target="_blank" rel="nofollow">PDF</a>, <a href="http://www.youtube.com/watch?v=xE4badDKL7c&feature=player_detailpage" target="_blank">VIDEO</a></div>
</section>

<section>
<DIV><span style="font-size: 14pt"><strong>Finger Vein Recognition</strong></span></DIV>
  <div><img border="0" height="178" src="https://sites.google.com/site/youngkyoonjang/_/rsrc/1353393493620/research/finger_vein_recognition.bmp?height=320&width=320" width="178"></div>
  <div>With increases in recent security requirements, biometric images such as fingerprints, faces and irises have been widely used in many recognition applications including door access control, personal authentication for computers, internet banking, automatic teller machines and border-crossing controls. Finger vein recognition uses the unique patterns of finger veins to identify individuals at a high level of accuracy. This paper proposes new devices and algorithms for touchless finger vein recognition.</div>
  <div></div>
  <div><br>
    Papers:</div>
  <div> - Journal of Information Processing Society (B) (2008, in Korean): <a href="https://docs.google.com/viewer?a=v&pid=sites&srcid=ZGVmYXVsdGRvbWFpbnx5b3VuZ2t5b29uamFuZ3xneDo2OWE5ZTE1MmM5ZDdhYzBl" target="_blank">PDF</a></div>
</section>

<section>
<DIV><span style="font-size: 14pt"><strong>Iris Recognition with eyelid localization on a mobile device</strong></span></DIV>
  <div><img border="0" height="127" src="https://sites.google.com/site/youngkyoonjang/_/rsrc/1353390618122/research/eyelid_localization.png?height=231&width=320" width="177"></div>
  <div>We propose a new portable iris recognition system. Because existing portable iris systems use customized embedded processing units, they are limited in ability to expand to other applications, and they have low processing power. To overcome such problems, we propose a new portable iris recognition system consisting of a conventional ultra-mobile personal computer (UMPC), a small universal serial bus (USB) iris camera, and near-infrared (NIR) light illuminators.</div>
  <div> </div>
  <div>Papers:</div>
  <div> - International Journal of Control, Automation and Systems (2010): <a href="http://link.springer.com/content/pdf/10.1007%2Fs12555-010-0112-0" target="_blank" rel="nofollow">PDF</a></div>
  <div> - Pattern Recognition Letters (2008): <a href="http://pdn.sciencedirect.com/science?_ob=MiamiImageURL&_cid=271524&_user=170364&_pii=S0167865508001578&_check=y&_origin=article&_zone=toolbar&_coverDate=01-Aug-2008&view=c&originContentFamily=serial&wchp=dGLbVlS-zSkWA&md5=273bb02f1461d0a03dcfd7210b855553&pid=1-s2.0-S0167865508001578-main.pdf" target="_blank" rel="nofollow">PDF</a></div>
</section>

    </div>
    <footer>
      <p>Project maintained by <a href="https://github.com/youngkyoonjang">youngkyoonjang</a></p>
      <p>Hosted on GitHub Pages &mdash; Theme by <a href="https://github.com/orderedlist">orderedlist</a></p>
    </footer>
    <!--[if !IE]><script>fixScale(document);</script><![endif]-->
    
  </body>
</html>
